# Основы программирования 1ПМ осень 2021

Лекция 1  
https://www.youtube.com/watch?v=6z-D8akDzD4

Лекция 2  
https://www.youtube.com/watch?v=U9On5Dn8JTE

Дополнительные материалы по С++ (просто и понятно)  
https://ravesli.com/uroki-cpp/#toc-0

Дополнительные материалы для тех, кто хочет чуть-чуть вперед  
https://www.youtube.com/watch?v=2PM4TgCZIQs&list=PLys0IdlMg6XcwfGUf9Z5QUjKH9iM1T8PG

Скачать Visual Studio 2019 Community  
https://visualstudio.microsoft.com/ru/vs/

